##Dokumentasjon

###Axios:
For å ta i bruk AJAX har vi brukt et JavaScript-bibliotek som heter Axios. Dette er fordi flere av gruppemedlemmene har hatt positiv erfaring med dette biblioteket tidligere. Fordelene er hovedsakelig at biblioteket automatisk konverterer til JSON, og at det er lett å sette opp og lett å bruke.

###Localstorage:
Vi valgte å bruke de to HTML Web Storage-variantene vi skulle bruke i denne oppgaven på litt forskjellige måter. Blant annet brukte vi local storage til å lagre projectId og token fra innloggingssiden. Ved å tillate at brukerne kan skrive inn både token og prosjektID vil appen ha et mye bredere bruksområde. For enkelhets skyld valgte vi også å inkludere mulighet for at brukeren kan bruke våre data. Disse ble anonymisert for å gi en objektiv vurdering av prosjektet.  Av denne årsaken lagret vi både token og prosjektID i local storage, slik at alle kallene i appen sender til riktig prosjekt.

###Sessionstorage:
Sessionstorage brukte vi på siden der man kan sortere og filtrere en liste over alle issues, og vi brukte det for å lagre hva issuesene var sortert etter. Vi tenkte dette var en god løsning, siden man da kan beholde sorteringen dersom man går ut av og inn i siden i samme fane. Vi tenkte at noe mer grundig lagring enn dette av sorteringen ikke var nødvendig, siden det er nokså enkelt å finne tilbake til den gamle sorteringen.

###Layout (media-queries):
Vi har brukt flere elementer for å tilpasse sidene våre slik at de skal få et fint design på ulike skjermstørrelser. Blant annet har vi brukt media-queries for å tilpasse designet slik at det ser bra ut på både mobil og pc. Dette gjorde at vi kan ha ulike størrelsesforhold og ulikt layout på ulike skjermstørrelser. Dette gjør at appen både har et estetisk bra design på mobil og på en pc-skjerm. Bruken av CSS-grid har også gjort sidene mer oversiktelig.

###Testing av layout:
Vi har testet hvordan nettsiden ser ut på flere ulike skjermstørrelser gjennom hele utviklingsprosessen. Vi har testet på vanlige pc-skjermer med ulike størrelser, i tillegg til mobil i både landscape og portrait mode. Da har vi testet at ting har et ønskelig størrelsesforhold, slik at ikke noe blir borte, eller blir for stort eller for lite i forhold til andre sideelementer. I tillegg har vi testet at funksjonaliteten fungerer uansett skjermstørrelse.

###Funksjonelle komponenter:
Vi har eksempler på både klasse- og funksjonelle komponenter i React-koden vår. Vi har likevel flere funksjonelle komponenter, siden gruppen er mer komfortable med dette, og vi mener at det er dette som er mest i vinden, og derfor mest relevant for bruk fremover. Likevel var det lærerikt å erfare at man kan bruke klassekomponenter, for å se at ting kan gjøres på ulike måter.

###Git:
Vi har, som oppgaven krevde, brukt git til utviklingen av nettsiden. Vi har laget issues for ting som skal fikses, og laget en egen branch per issue. Før vi har merget en branch inn i master, har vi fått et av de andre gruppemedlemmene til å se over koden. Dette gjør at man har større sjanse for å oppdage eventuell tunglest kode, i tillegg til at man får et bedre innblikk i hele prosjektet ved å sette seg inn i andres kode.

P###arametrisering:
Vi lar brukeren gjøre flere ulike valg på hvilken type informasjon som vises om både issues og commits. Vi har viser informasjon både i form av list-view og grafer. På list-view-sidene kan man både sortere og filtrere på ulike krav. Listen tilpasses da etter hva man ønsker å se, og i hvilken rekkefølge man ønsker å se elementene. På graf-sidene kan man endre hva man ønsker å se på aksene, slik at man får flere ulike måter å vise ulik informasjon på. I tillegg har vi en oversikt over antall issues og commits per person i form av kakediagram. Her kan man velge hvilke brukere man kan se antallet commits og issues på.

###Props, state og context:
Props og state har vi brukt der vi mener det er hensiktsmessig. Vi har blant annet brukt state på listen over issues, for at den skal endre seg når man endrer sortering/filtrering. Props brukes f.eks. for å ta inn data i SingleCommit. Context har vi brukt på siden som viser grafer av commits. Det bruker vi for å kunne endre språk på siden.

###Testing (Jest og Enzyme):
Til testing har vi brukt Jest og Enzyme. Jest gjør testingen både enkel å skrive og å utføre. Jest er også et raskt testrammeverk og lett å mocke. Til prosjektet fungerte dette derfor optimalt. Enzyme er et bibliotek for å lett teste enkeltkomponenter. Dette gjør ikke Jest like bra og de er derfor en god kommuniksjon. Ettersom Enzyme kun rendrer komponenten og barn, er det også et veldig raskt rammeverk.

###Viewport:
Vi har valgt å ikke bruke viewport på applikasjonen vår. Vi implementerte det ikke fra starten av, men brukte heller andre metoder som media-queries for å tilpasse sidene til ulike skjermstørrelser. Da vi implementerte det på slutten av prosjektet, så vi at det ikke utgjorde noen forskjell for layoutet på siden vår, siden vi allerede hadde tatt hensyn til ulike skjermstørrelser. Derfor valgte vi å fjerne det igjen, og det er derfor ikke med på det endelige produktet. 

### Error meldinger i konsollen:
Det oppstår en del error meldinger i konsollen som følge av rendering av Victory sitt BarChart. Det ser ut til at y verdier på 0 tolkes som Not a Number. Det er gjort en del debugging for å forstå feilen, men har ikke greid å fikse feilen. Den ser imidlertidig ikke ut til å påvirke utseende eller funksjonaliteten til diagrammet.

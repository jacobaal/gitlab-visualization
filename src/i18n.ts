// Source: https://react.i18next.com/guides/quick-start

import i18n from "i18next";
import { initReactI18next } from "react-i18next";

// the translations
// (tip move them in a JSON file and import them,
// or even better, manage them separated from your code: https://react.i18next.com/guides/multiple-translation-files)
const resources = {
  en: {
    translation: {
      "Charts for issues": "Charts for issues",
      "Charts for commits": "Charts for commits",
      "Start date": "Start date",
      "End date": "End date",
      "Commits per day": "Commits per day",
      "Commits per member": "Commits per member",
      "Issues authored per member": "Issues authored by each member",
      "Tiger": "Tiger",
      "Lion": "Lion",
      "Giraffe": "Giraffe",
      "Rhino": "Rhino",
    }
  },
  no: {
    translation: {
      "Charts for issues": "Diagrammer for issues",
      "Charts for commits": "Diagrammer for commits",
      "Start date": "Startdato",
      "End date": "Sluttdato",
      "Commits per day": "Commits per dag",
      "Commits per member": "Commits per medlem",
      "Issues authored per member": "Issues laget av hvert medlem",
      "Tiger": "Tiger",
      "Lion": "Løve",
      "Giraffe": "Sjiraff",
      "Rhino": "Nesehorn",
    }
  }
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: "en", // language to use, more information here: https://www.i18next.com/overview/configuration-options#languages-namespaces-resources
    // you can use the i18n.changeLanguage function to change the language manually: https://www.i18next.com/overview/api#changelanguage
    // if you're using a language detector, do not define the lng option

    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

  export default i18n;
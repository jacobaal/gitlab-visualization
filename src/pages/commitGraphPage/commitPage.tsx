
import { BarChart } from '../../components/graphs/barChart/barChart';
import { PieChart } from '../../components/graphs/pieChart/pieChart';
import { useGitlabApi } from '../../utils/gitlab_api_service';
import styles from './commitPage.module.css';
import { queryTypes, Commit } from '../../utils/queryType'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";
import { useCommitContext } from '../../context/commitPageContext';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import GobackButton from '../../components/goBack/goBackButton';

export const CommitPage = () => {
    const commitData = useGitlabApi(queryTypes.AllCommitsWithoutPagination);
    const { t, i18n } = useTranslation();
    const context = useCommitContext();
    
    useEffect(() => {
        i18n.changeLanguage(context.language)
    }, [context.language])

    if (commitData.isLoading) {
        return (
            <Loader
                type="Puff"
                color="#00BFFF"
                height={100}
                width={100}
                timeout={3000}
            />
        );
    }

    return (
        <div className={styles.pageContainer}>
            <div className={styles.header}>
                <GobackButton/>
                <h1>{t("Charts for issues")}</h1>
                <button
                    className={styles.button}
                    onClick={() => context.setLanguage(context.language === "no" ? "en" : "no")}>
                    {context.language}
                </button>
            </div>
            <BarChart data={commitData.data as Commit[]} title="Commits per day"/>
            <PieChart data={commitData.data as Commit[]} title={"Commits per member"}/>
        </div>
    )
}

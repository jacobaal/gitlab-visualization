import React from 'react';
import IssuesListComponent from "../../components/issuesListComponent/issuesListComponent";
import styles from "./issuePage.module.css";
import {useGitlabApi} from "../../utils/gitlab_api_service";
import {queryTypes, Issue} from "../../utils/queryType";
import ClipLoader from "react-spinners/ClipLoader";
import GobackButton from '../../components/goBack/goBackButton';

const IssuePage = () => {

    const {isLoading, data} = useGitlabApi(queryTypes.AllIssues)
    return (
        <div className={styles.container}>
            <GobackButton/>
            <h1 className={styles.headline}> Issues </h1>
            {isLoading ? <ClipLoader loading={isLoading}/> : <IssuesListComponent issues={data! as Issue[]} />}
        </div>

    )
}

export default IssuePage;
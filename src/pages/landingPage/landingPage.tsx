import React, { useEffect, useState} from "react"
import style from './landingPage.module.css';
import {useHistory} from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ConnectionTest, queryTypes } from "../../utils/queryType";
import { useGitlabApi } from "../../utils/gitlab_api_service";
import visualization from "../../assets/data_visualization.jpeg";


const LandingPage = () => {
    let history = useHistory()
    const [key, setKey] = useState<string>(localStorage.getItem("key") ?? "");
    const [projectId, setProjectId] = useState<string>(localStorage.getItem("projectId") ?? "");
    const [query,setQuery] = useState<queryTypes>()
    const {data, isLoading,error} = useGitlabApi(query)
    const [disableButton, setDisableButton] = useState<boolean>(true);

    const toast_response = (type:string) => {
        if(type === "info"){
            toast.info("Checking if key and projectId is correct");
        }
        else if (type==="success") {
            toast.success("Success! Found the repository");
        }
        else {
            toast.error("Could not find repository or wrong key. Try again");
        }
    }

    const setDefaultValues = ()=> {
        localStorage.clear();
        history.push("/overview");
    }

    useEffect(()=>{
            if(!(projectId && key)) return;
            const timer = setTimeout(()=>{
                setDisableButton(true)
                localStorage.setItem("projectId", projectId);
                localStorage.setItem("key", key);
                toast_response("info")
                setQuery(queryTypes.Access);
            }, 300)
        return () =>{
            setQuery(undefined);
            clearTimeout(timer);
        }
    }, [key, projectId]);

    useEffect(() => {
        if(!isLoading){
            if(error){
                toast_response("error")
            }
            else if(data as ConnectionTest) {
                toast_response("success")
                setDisableButton(false);
            }
        }
    }, [data, error, isLoading]);

    function handleSubmit(event: { preventDefault: () => void; }) {
        event.preventDefault()
        history.push("/overview")
    }


    return (
        <div className={style.inputWrapper}>
            <img className={style.picture} alt="visualization" src={visualization} style={{width: "80%", margin: "30px auto" }}/>
            <form onSubmit={handleSubmit}>
                <input className={style.inputField}  value = {projectId} type="Text" onChange={e => setProjectId(e.target.value)} id="ProjektIDInput"
                       placeholder="ProjectId" required/>
                <input className={style.inputField} value = {key} type="Text" onChange={e => setKey(e.target.value)} id="KeyInput"
                       placeholder="Key" required/>
                <div className={style.buttonDiv}>
                    <button className={style.defaultButton} onClick={()=> setDefaultValues()}>Use default values</button>
                    <button className={style.submitButton} type="submit" disabled={disableButton}>Use current values</button>
                </div>
            </form>
            <ToastContainer autoClose={2000} />

        </div>
    );
};

export default LandingPage;
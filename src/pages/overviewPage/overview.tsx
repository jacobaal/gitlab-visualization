import style from './overview.module.css';
import { useHistory } from 'react-router';
import { useGitlabApi } from '../../utils/gitlab_api_service';
import { Languages, queryTypes } from '../../utils/queryType';
import StatsBox from '../../components/stats/statsBox';
import { ClipLoader } from 'react-spinners';
import GobackButton from '../../components/goBack/goBackButton';

const OverviewPage = () => {
    const slug = window.location.pathname;
    const history = useHistory();
    const {data, isLoading} = useGitlabApi(queryTypes.Languages);
    

    return (
        <div className={style.wrapper}>
            <GobackButton/>
            {slug==="/overview" ?<h1>Stats:</h1>: null}
            {isLoading ? <ClipLoader loading={isLoading}/> : 
                slug==="/overview" ?<StatsBox content={data as Languages ? Object.entries(data as Languages).map(element => `${element[0]}: ${element[1]} %`): ["Loading ..."] } />
           :null}
            <h2 style={{marginTop: "30px"}}>{slug=== "/overview" ?"More info:": "Choose display form:"}</h2> 
            <div className={style.buttonWrapper}>
            <button className={style.button} onClick= {() => history.push(slug=== "/overview" ? "/issue" : `${slug}graph`)}>
            {slug=== "/overview" ? "Issues" : "Graph"}
            </button>
            <button className={style.button} onClick= {() => history.push(slug=== "/overview" ? "/commit" : `${slug}list`)}>
            {slug=== "/overview" ? "Commits" : "List"}
            </button>
            </div>
        </div>
    );
};

export default OverviewPage;
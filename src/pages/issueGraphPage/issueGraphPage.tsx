import Loader from 'react-loader-spinner';
import GobackButton from '../../components/goBack/goBackButton';
import { BarChart } from '../../components/graphs/barChart/barChart';
import { PieChart } from '../../components/graphs/pieChart/pieChart';
import { useGitlabApi } from '../../utils/gitlab_api_service';
import { Issue, queryTypes } from '../../utils/queryType';
import styles from './issueGraphPage.module.scss';


export const IssueGraphPage = () => {

    const commitData = useGitlabApi(queryTypes.AllIssuesWithoutPagination);

    if (commitData.isLoading) {
        return (
            <Loader
                type="Puff"
                color="#00BFFF"
                height={100}
                width={100}
                timeout={3000}
            />
        );
    }

    return (
        <div className={styles.pageContainer}>
            <GobackButton/>
            <h1>Charts for issues</h1>
            <BarChart data={commitData.data as Issue[]} title="Issues authored per day"/>
            <PieChart data={commitData.data as Issue[]} title={"Issues authored per member"}/>
        </div>
    )
}

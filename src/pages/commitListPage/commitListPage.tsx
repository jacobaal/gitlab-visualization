import React, {FC} from 'react';
import {Commit} from "../../utils/queryType";
import style from "./commitListPage.module.css";
import {anonymousAnimals} from "../../utils/victory/useVictory"


interface iSingleCommit {
    id: string,
    created_at: Date,
    title: string,
    message: string,
    author_name: string,
    committer_name: string,
}

const SingleCommit: FC<iSingleCommit> = (props) => {
    return (
        <div className={style.SingleCommit}>
            <h3>{props.title}</h3>
            <div className={style.Description}>
            </div>
            <div className={style.Description}>
                <b>Author: </b>
                <p>
                    {props.author_name} at {props.created_at.getUTCDate()}.{props.created_at.getUTCMonth() + 1}
                    - {props.created_at.getUTCHours() + 2}:{props.created_at.getUTCMinutes().toString().length === 2
                    ? props.created_at.getUTCMinutes() : "0" + props.created_at.getUTCMinutes().toString()}
                </p>
            </div>
        </div>)
}


interface commitProps {
    commits: Commit[]
}

const CommitComponent = (props: commitProps) => {
    let commit: Commit[] = props.commits
    let map = new Map<string,string>()
    let mapIndex=0

    function anonymize(name: string):string{
        if (map.has(name)){
            return map.get(name) as string
        }
        else{
            map.set(name,anonymousAnimals[mapIndex])
            mapIndex++
            return map.get(name) as string
        }
    }
    const commitList = commit?.map(c => <li><SingleCommit
        author_name={anonymize(c.author_name)}
        committer_name={c.committer_name}
        created_at={new Date(c.created_at)}
        id={c.id}
        message={c.message}
        title={c.title}/></li>)


    return (
        <div>
            <ul className={style.UlList}>
                {commitList}
                {commitList?.length === 0 ? <li>Opps! Too far, go back!</li> : <></>}
            </ul>
        </div>
    );
};

export default CommitComponent
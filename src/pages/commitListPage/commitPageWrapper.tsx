import React, {useEffect, useState} from 'react';
import CommitComponent from "./commitListPage"
import {useGitlabApi} from "../../utils/gitlab_api_service";
import {Commit} from "../../utils/queryType";
import style from "./commitListPage.module.css"
import Loader from "react-loader-spinner";
import GobackButton from '../../components/goBack/goBackButton';


const CommitPageWrapper = () => {

    const [pageNumber, setPageNumber] = useState(1)
    const [disablePrev, setDisablePrev] = useState(pageNumber === 1)
    const [disableNext, setDisableNext] = useState(false)

    let {isLoading, data} = useGitlabApi("repository/commits?per_page=20&page=" + pageNumber)


    function increase() {
        setPageNumber(prev => prev + 1)
    }

    function decrease() {
        if (pageNumber !== 1) {
            setPageNumber(prev => prev - 1)
        }
    }

    useEffect(() => {
        setDisablePrev(pageNumber === 1)
        setDisableNext((data as string)?.length === 0)
    }, [pageNumber, data])
    //circles, grid, Oval - standard,
    return (
        <div>
            <GobackButton/>
            <h1>Commits</h1>
            <div className={style.ArrowDiv}>
                <button className={style.Button} disabled={disablePrev} onClick={decrease}>
                    <i className={`${style.Arrow} ${style.Left}`}></i>
                </button>
                <h2>
                    {pageNumber}
                </h2>
                <button className={style.Button} disabled={disableNext} onClick={increase}>
                    <i className={`${style.Arrow} ${style.Right}`}></i>
                </button>

            </div>
            {isLoading ? <div className={style.LoadingIcon}><Loader
                type="TailSpin"
                color="#EB753C"
                height={100}
                width={100}
                timeout={10000} //3 secs
            /></div> : <CommitComponent commits={data as Commit[]}/>}
        </div>

    )
}


export default CommitPageWrapper;
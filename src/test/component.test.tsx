import { shallow } from "enzyme";
import GobackButton from "../components/goBack/goBackButton";
import Enzyme from 'enzyme';
import ReactSixteenAdapter from 'enzyme-adapter-react-16';
import OverviewPage from "../pages/overviewPage/Overview";

Enzyme.configure({ adapter: new ReactSixteenAdapter() });

const wrapped = shallow(<OverviewPage/>);
describe('OverviewPage', () => {
  it('should render the OverviewPage', () => {   
    expect(wrapped.find(".buttonWrapper").children().length).toEqual(2);
    expect(wrapped.find(".buttonWrapper").childAt(0).text()).toEqual("Graph");
    expect(wrapped.find(".buttonWrapper").childAt(1).text()).toEqual("List");
    expect(wrapped.find("h2").text()).toEqual("Choose display form:");
  });
});
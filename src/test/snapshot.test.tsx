import { shallow } from 'enzyme';
import App from '../App';
import Enzyme from 'enzyme';
import ReactSixteenAdapter from 'enzyme-adapter-react-16';
import IssuePage from '../pages/issueListPage/issuePage';
import { IssueGraphPage } from '../pages/issueGraphPage/issueGraphPage';
import OverviewPage from '../pages/overviewPage/overview';
import LandingPage from '../pages/landingPage/landingPage';
import CommitPageWrapper from '../pages/commitListPage/commitPageWrapper';
import { CommitPage } from '../pages/commitGraphPage/commitPage';

Enzyme.configure({ adapter: new ReactSixteenAdapter() });

test('renders the App Component', () => {
    const component = shallow(<App/>);
    expect(component).toMatchSnapshot();
  });
test('renders the IssueListpage', () => {
    const component = shallow(<IssuePage/>);
    expect(component).toMatchSnapshot();
  });
test('renders the IssueGraphPage', () => {
    const component = shallow(<IssueGraphPage/>);
    expect(component).toMatchSnapshot();
  });
test('renders the overviewPage', () => {
    const component = shallow(<OverviewPage/>);
    expect(component).toMatchSnapshot();
  });
test('renders the LandingPage', () => {
    const component = shallow(<LandingPage/>);
    expect(component).toMatchSnapshot();
  });
test('renders the CommitListPage', () => {
    const component = shallow(<CommitPageWrapper/>);
    expect(component).toMatchSnapshot();
  });
test('renders the CommitGraphPage', () => {
    const component = shallow(<CommitPage/>);
    expect(component).toMatchSnapshot();
  });



import { createContext, Dispatch, SetStateAction, useContext } from "react"
import '../i18n';
export type GlobalGraphCommit = {
  language: string,
  setLanguage: Dispatch<SetStateAction<string>>,
}

export const GlobalCommitContext = createContext<GlobalGraphCommit>({
  language: "no",
  setLanguage: () => {},
});
export const useCommitContext = () => useContext(GlobalCommitContext)

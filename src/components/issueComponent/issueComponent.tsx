import React from "react";
import styles from "../issueComponent/issueComponent.module.css";
import {Issue} from "../../utils/queryType";


class IssueComponent extends React.Component<{ item: Issue }>{
    render() {
        const dateCreated = new Date(this.props.item.created_at);
        return <div className={styles.issue}>
                    <p className={styles.title}> {this.props.item.title} </p>
                    <p className={styles.description}> {this.props.item.description} </p>
                    <p className={styles.state}> {"State: " + this.props.item.state} </p>
                    <p className={styles.date}> {"Date created: " + dateCreated.getDate() + "." + dateCreated.getMonth()} </p>
                </div>
    }
}

export default IssueComponent;
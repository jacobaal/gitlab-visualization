import React from 'react';
import { useHistory } from 'react-router';
import style from "./goBackButton.module.css"

const GobackButton = () => {
    const history = useHistory();
    return(

        <button className={style.button} onClick={()=> history.goBack()}>Go back</button>
        );
};

export default GobackButton;
import { useEffect, useState } from "react";
import { VictoryBar, VictoryChart, VictoryTheme } from "victory";
import { useVictory } from "../../../utils/victory/useVictory";
import styles from './barChart.module.scss';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import { ICommitsPerDay } from "../../../utils/victory/types";
import { Commit, Issue } from '../../../utils/queryType';
import { TextField } from "@mui/material";
import { DatePicker } from "@mui/lab";
import { useTranslation } from 'react-i18next';


interface IBarChartProps {
    data: Commit[] | Issue[],
    title: string,
}


export const BarChart = (props: IBarChartProps) => {

    const { getEntriesPerDayBarChartData } = useVictory(props.data);
    const [currentData, setCurrentData] = useState<ICommitsPerDay[]>();
    const [dateIntervalStart, setStartDate]  = useState<Date>(new Date("2021-09-26"));
    const [dateIntervalEnd, setEndDate]  = useState<Date>(new Date("2021-10-03"));
    const { t } = useTranslation();

    useEffect(() => {
        let commitsPerDay = getEntriesPerDayBarChartData(dateIntervalStart, dateIntervalEnd);
        setCurrentData(commitsPerDay);
    }, [dateIntervalStart, dateIntervalEnd])

    return (
          <div className={styles.container}>
            <h2>{t(props.title)}</h2>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
                <div className={styles.datePickerContainer}>
                    <div>
                        <DatePicker
                            label={t("Start date")}
                            value={dateIntervalStart}
                            onChange={(newValue) => {
                            setStartDate(newValue ?? new Date("2021-09-26"));
                            }}
                            renderInput={(params) => <TextField {...params} sx={{ marginBottom: "10px"}} />}
                            />
                    </div>
                    <div>
                        <DatePicker
                            label={t("End date")}
                            value={dateIntervalEnd}
                            onChange={(newValue) => {
                            setEndDate(newValue ?? new Date());
                            }}
                            renderInput={(params) => <TextField {...params} />}
                            />
                    </div>
                </div>
            </LocalizationProvider>
            <div className={styles.wrapper}>
              <VictoryChart
                domainPadding={20}
                theme={VictoryTheme.material}
                style={{ background: { fill: '#3d3d3d'}}}>
                <VictoryBar
                    data={currentData}
                    x="date"
                    y="amount"
                    style={{ data: {fill: 'orange'}}}/>
              </VictoryChart>
          </div>
        </div>
    )
}

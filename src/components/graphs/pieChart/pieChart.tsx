import { useEffect, useState } from 'react';
import { VictoryContainer, VictoryPie } from 'victory';
import { Commit, Issue } from '../../../utils/queryType';
import { useVictory } from '../../../utils/victory/useVictory';
import styles from './pieChart.module.scss';
import Switch from "react-switch";
import { useTranslation } from 'react-i18next';

interface IPieChartProps {
    data: Commit[] | Issue[],
    title: string,
}

export const PieChart = (props: IPieChartProps) => {

    const { getEntriesPerMemberPieChartData, getAnonAnimals, colorScaleAnimals } = useVictory(props.data);
    const [selectedAnimals, setSelectedAnimals] = useState(
        getAnonAnimals().map(animal => { return {animal: animal, selected: true}}))
    const [currentData, setCurrentData] = useState(getEntriesPerMemberPieChartData(selectedAnimals));
    const { t } = useTranslation();

    const changeSelectedAnimals = (animalToChange: string) => {
        setSelectedAnimals(prevState =>
            prevState.map(prevEntry => {
                if (prevEntry.animal === animalToChange) {
                    prevEntry.selected = !prevEntry.selected
                }
                return prevEntry
            }))
    }

    const animalCheckboxes = () : JSX.Element[] => {
        let checkboxes = getAnonAnimals().map(animal => {
            return (
                <div className={styles.checkboxContainer} key={animal}>
                    <label>{t(animal)}</label>
                    <Switch
                        onColor="#fae7c2"
                        checked={selectedAnimals.find(sa => sa.animal === animal)?.selected ?? false}
                        onChange={() => changeSelectedAnimals(animal)}
                        />
                </div>
            );
        })
        return checkboxes;
    }

    useEffect(() => {
        setCurrentData(getEntriesPerMemberPieChartData(selectedAnimals))
    }, [selectedAnimals])

    return (
        <div className={styles.pieChartContainer}>
            <h2>{t(props.title)}</h2>
            <div className={styles.inputContainer}>
                {animalCheckboxes()}
            </div>
                <VictoryPie
                    data={currentData[0]}
                    containerComponent={<VictoryContainer responsive={true}/>}
                    x="member"
                    y="amount"
                    colorScale={colorScaleAnimals}
                    />
        </div>
    );
}


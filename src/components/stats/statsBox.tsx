import React from 'react';
import style from "./stats.module.css";

interface StatsBoxProps {
    content: string[]
}

const StatsBox: React.FC<StatsBoxProps> = ({content }) => {
    return (
        <div className={style.wrapperStats}>
            {content.map(element => <p key={element} style={{margin: "10px auto", padding: "10px auto"}}>{element}</p>)}
        </div>
    );
};

export default StatsBox;
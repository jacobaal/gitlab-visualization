import React, {useState} from 'react';
import styles from './issuesListComponent.module.css'
import {Issue} from "../../utils/queryType";
import Select, {SingleValue} from 'react-select';
import IssueComponent from "../issueComponent/issueComponent";

interface issuesProps {
    issues: Issue[]
}

const sortingOptions = [
    {label: "Time created (asc)", value: "timeAsc"},
    {label: "Time created (desc)", value: "timeDesc"},
    {label: "Title (asc)", value: "alphAsc"},
    {label: "Title (desc)", value: "alphDesc"}
]

const filteringOptions = [
    {label: "Opened", value: "opened"},
    {label: "Closed", value: "closed"}
]

const IssuesListComponent = (props: issuesProps) => {

    const myStorage = window.sessionStorage;
    const [sortedIssues, setSortedIssues] = useState<Issue[]>(sortIssues());

    function sortIssues() {
        const copy =  props.issues ? [...props.issues] : [];
        const selected = myStorage.getItem("selected");

        if (selected === "timeAsc"){
            copy.sort((a: Issue, b: Issue) => {
                return new Date(a.created_at).getTime() - new Date(b.created_at).getTime() ? -1 : 1
            });
        }
        else if (selected === "timeDesc"){
            copy.sort((a: Issue, b: Issue) => {
                return new Date(a.created_at).getTime() - new Date(b.created_at).getTime() ? 1 : -1
            });
        }
        else if (selected === "alphAsc"){
            copy.sort((a: Issue, b: Issue) => {
                return a.title > b.title ? 1 : -1
            });
        }
        else if (selected === "alphDesc"){
            copy.sort((a: Issue, b: Issue) => {
                return a.title > b.title ? -1 : 1
            });
        }

        return copy

    }

    function filterIssues(event: SingleValue<{label: string, value: string}>) {
        const copy : Issue[]= [...props.issues];
        event !== null ? setSortedIssues(copy.filter(x => x.state === event.value)) : setSortedIssues(copy);
    }

    const selectChange = (event: SingleValue<{label: string, value: string}>) => {

        if (event != null) {
            myStorage.setItem("selected", event.value)
            setSortedIssues(sortIssues())
        }

    };

    function setPlaceholder() {
        const ar : {label: string, value: string} | undefined = sortingOptions.find(x => x.value === myStorage.getItem("selected"));
        return ar !== undefined ? ar.label : "Time created (desc)";
    }

    return (
        <div className={styles.comp}>
            <p>Sort by:</p>
            <Select className={styles.dropdown} options={sortingOptions} onChange={e => selectChange(e)} placeholder={setPlaceholder()}/>
            <p>Filter by:</p>
            <Select className={styles.dropdown} options={filteringOptions} onChange={e => filterIssues(e)} isClearable={true}/>
            {sortedIssues.length === 0 ? "You have noe issues" : sortedIssues.map((item, key) => (
                <IssueComponent key={key} item={item}/>
            ))}
        </div>
    );
};

export default IssuesListComponent;
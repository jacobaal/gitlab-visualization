
import {Redirect, Route, Switch} from 'react-router';
import LandingPage from "./pages/landingPage/landingPage";
import { GlobalCommitContext } from './context/commitPageContext';
import IssuePage from './pages/issueListPage/issuePage';
import { CommitPage } from './pages/commitGraphPage/commitPage';
import { IssueGraphPage } from './pages/issueGraphPage/issueGraphPage';
import CommitPageWrapper from "./pages/commitListPage/commitPageWrapper"
import './i18n';
import { useState } from 'react';
import OverviewPage from './pages/overviewPage/overview';

function App() {

    const [language, setLanguage] = useState("no");

    return (
        <div className="App">
            <Switch>
                <Route exact path={"/"}>
                    <LandingPage/>
                </Route>
                <Route exact path={"/overview"}>
                    <OverviewPage/>
                </Route>
                <Route exact path={"/issue"}>
                        <OverviewPage/>
                </Route>
                <Route exact path={"/commit"}>
                        <OverviewPage/>
                </Route>
                <Route exact path={"/issuelist"}>
                    <IssuePage />
                </Route>
                <Route exact path={"/issuegraph"}>
                    <IssueGraphPage />
                </Route>
                <Route exact path={"/commitlist"}>
                    <CommitPageWrapper/>
                </Route>
                <Route exact path={"/commitgraph"}>
                    <GlobalCommitContext.Provider value={{language: language, setLanguage: setLanguage}}>
                        <CommitPage />
                    </GlobalCommitContext.Provider>
                </Route>
                <Redirect to={"/"}/>
            </Switch>
        </div>
    );
}

export default App;

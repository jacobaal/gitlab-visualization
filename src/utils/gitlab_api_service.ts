import axios, { AxiosError } from "axios";
import { useCallback, useEffect, useState } from "react";
import { Branch, Issue, Commit, ConnectionTest, queryTypes, Languages } from "./queryType";

export const useGitlabApi = (query: queryTypes|undefined|string) =>  {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState<AxiosError | undefined>();
    const [data, setData] = useState<Issue[] | Commit[] | Branch[] | ConnectionTest | Languages>();
    const projectId = localStorage.getItem("projectId") ?? "11916"
    const apiKey = localStorage.getItem("key") ??  "r74NY2kS_LprPFEobZxi";
    const url = `https://gitlab.stud.idi.ntnu.no/api/v4/projects/${projectId}/${query}`;
    
    const sendRequest = useCallback(async () => {
        setIsLoading(true);
        setError(undefined);
        try{
          const response = await axios.get(url, 
          {headers: {"Authorization": `Bearer ${apiKey}`, "content_type":"application/json"}},);
          setData(response.data);
          setIsLoading(false);
      }
      catch (e){
          setError(e as AxiosError);
          setIsLoading(false);
      }}, [apiKey, url]);

      useEffect(() => {
          if(!query) return;
        sendRequest();
      },[query, sendRequest]);

    return {
        isLoading: isLoading,
        error: error,
        data: data,
    }
};
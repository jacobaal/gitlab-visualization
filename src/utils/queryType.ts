export enum queryTypes {
    AllBranches = "repository/branches/",
    MergedBranches = "repository/merged_branches/",
    AllCommits = "repository/commits?page=1",
    AllIssues = "issues/",  
    Languages = "languages",  
    Access="access_requests",
    AllIssuesWithoutPagination = "issues/?scope=all",
    AllCommitsWithoutPagination = "repository/commits/?per_page=1000",
}

export type User = {
    id: String,
    name: String, 
    username: String,
}

export type Issue = {
    id: number,
    project_id: number,
    title: string,
    description: string,
    state: string, 
    created_at: Date,
    updated_at: Date,
    labels: String[],
    author: User,
    assignee: User
}

export type Commit = {
    id: string,
    created_at: Date,
    title: string,
    message: string,
    author_name: string,
    committer_name: string,
    committed_date: Date,
}

export type Branch = {
    name: string,
    commit: Commit,
    merged: boolean,
    web_url: string
}


export type ConnectionTest = {
};

export type Languages = {
    Typescript: number,
    CSS: number,
    HTML: number,
}
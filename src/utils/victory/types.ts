interface IGraphData {
    name: string,
    date: Date,
}

interface ICommitsPerDay {
    date: string,
    amount: number,
}

interface ICommitsPerMember {
    member: string,
    amount: number,
}

export type {IGraphData, ICommitsPerDay, ICommitsPerMember};
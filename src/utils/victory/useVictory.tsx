import { useState } from "react";
import { Commit, Issue } from "../queryType";
import { IGraphData, ICommitsPerDay, ICommitsPerMember } from "./types";

export const anonymousAnimals = [
    "Tiger",
    "Lion",
    "Giraffe",
    "Rhino",
    "Monkey",
    "Donkey",
    "Zebra",
    "Alligator",
    "Baboon",
    "Gorilla",
    "Lemur",
]

const colorScaleAnimals = [
    "#f0ad34",
    "#f1b74c",
    "#f3c063",
    "#f5ca7b",
    "#f7d493",
    "#f9ddaa",
    "#fae7c2",
]


export const useVictory = (initData: Commit[] | Issue[]) => {

    const [data, setData] = useState(anonymizeData(initData));

    function anonymizeData(currentData: Commit[] | Issue[]) {
        let emails: string[] = []
        let animals: string[] = []

        let standardizedData = currentData?.map(entry => {
            return {
                name: (entry as Commit).committer_name ?? (entry as Issue).author.username,
                date: (entry as Commit).committed_date ?? (entry as Issue).created_at,
            }
        })

        standardizedData?.forEach(entry => {
            if (!emails.includes(entry.name)) {
                emails.push(entry.name)
            }
        });
        
        let anonData : IGraphData[] = []

        standardizedData?.forEach(entry => {
            let memberNumber = emails.indexOf(entry.name);
            let anonName = memberNumber < anonymousAnimals.length
                ? anonymousAnimals[memberNumber]
                : memberNumber.toString();
            if (!animals.includes(anonName)) animals.push(anonName);
            anonData.push({ name: anonName, date: new Date(entry.date)});
        })
        
        return { anonData, animals };
    }

    function getEntriesPerDayBarChartData(startDateInput: Date, endDateInput: Date) : ICommitsPerDay[]{
        let commitsPerDayData = []
        let anon = anonymizeData(initData);

        let startDate = new Date(startDateInput);
        let endDate = new Date(endDateInput);

        for (let day = startDate; day <= endDate; day.setDate(day.getDate() + 1)) {
            let amount: number = anon.anonData.filter(entry =>
                entry.date.getMonth() === day.getMonth() && entry.date.getDate() === day.getDate()
                ).length;
            commitsPerDayData.push({ date: `${day.getDate()}.${day.getMonth() + 1}`, amount: amount});
        }
        return commitsPerDayData;
    };

    function getEntriesPerMemberPieChartData(animals: { animal: string, selected: boolean}[]) {
        let commitsPerMemberData: ICommitsPerMember[] = []

        animals.filter(animal => animal.selected).forEach(animal => {
            let amount: number = data.anonData.filter(entry => entry.name === animal.animal).length;
            commitsPerMemberData.push({ member: animal.animal, amount: amount})
        })

        return [commitsPerMemberData, animals];
    }

    function getAnonAnimals() {
        return data.animals;
    }

    return { getEntriesPerDayBarChartData, anonymizeData, getEntriesPerMemberPieChartData, getAnonAnimals, colorScaleAnimals };
}
